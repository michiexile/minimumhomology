import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.sparse, scipy.sparse.linalg

eg = read3d.readply('ply/egret.ply')
egV = eg[0]
egP = eg[1][:,1:]
egE = minimumhomology.edges(egP)


c1 = [917, 916, 915, 912, 913, 914, 917]
c2 = [937, 938, 941, 940, 939, 936, 937]
c3 = [432, 430, 436, 444, 452, 455, 448, 440, 432]


z1 = zeros((len(egE),))
z2 = zeros((len(egE),))
z3 = zeros((len(egE),))

z1p = []
z2p = []
z3p = []

for i in range(len(c1)-1):
    if c1[i] < c1[i+1]:
        z1[egE.index((c1[i],c1[i+1]))] = 1
        z1p.append((c1[i],c1[i+1]))
    else:
        z1[egE.index((c1[i+1],c1[i]))] = -1
        z1p.append((c1[i+1],c1[i]))
    

for i in range(len(c2)-1):
    if c2[i] < c2[i+1]:
        z2[egE.index((c2[i],c2[i+1]))] = 1
        z2p.append((c2[i],c2[i+1]))
    else:
        z2[egE.index((c2[i+1],c2[i]))] = -1
        z2p.append((c2[i+1],c2[i]))

for i in range(len(c3)-1):
    if c3[i] < c3[i+1]:
        z3[egE.index((c3[i],c3[i+1]))] = 1
        z3p.append((c3[i],c3[i+1]))
    else:
        z3[egE.index((c3[i+1],c3[i]))] = -1
        z3p.append((c3[i+1],c3[i]))


egD = minimumhomology.boundaryedges(egP,egE)
chain = minimumhomology.boundingchain(egD,z1,z2)

outside = abs(chain).max()
norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
cols = cm.RdBu(norm(chain))
read3d.draw(egV,egP,facecolors=cols,linewidths=0.2)

read3d.draw(egV,z1p,edgecolors=(1,0,0),linewidths=2)
read3d.draw(egV,z2p,edgecolors=(0,1,0),linewidths=2)

