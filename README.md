# README #


### Computing minimum area homology ###

This is the companion repository to the paper "Computing minimum area homology", to appear in Computer Graphics Forum.

The included python code depends on SciPy, and improves in capacity if networkx or shapely are available. 
