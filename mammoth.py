import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.linalg, scipy.sparse, scipy.sparse.linalg
import matplotlib

eg = read3d.readply('smithsonian/simplemammoth.ply')
egV = eg[0]
egP = eg[1][:,1:]
egE = minimumhomology.edges(egP)
try: 
    egA = minimumhomology.areas(egP, egV)
except NotImplementedError:
    egA = scipy.loadtxt('smithsonian/simplemammoth.area')

egA[abs(egA) < 1e-5] = 1e-5*egA[abs(egA) > 1e-5].min()
egAi = 1./scipy.array(egA)
egAi[scipy.isinf(egAi)] = 0

cs = []
# Mammoth cycle near end of backbone:
cs.append([2415, 332, 563, 2018, 2280, 922, 1821, 2019, 2772, 1790, 2415])

# Mammoth cycles near middle of backbone, in ribs:
cs.append([2081, 3891, 2984, 960, 4277, 4276, 4279, 3889, 2378, 2081])

# Cycle on mammoth neck:
cs.append([385, 920, 2077, 3225, 2364, 2365, 2968, 4336, 578, 385])

# Two cycles one of the tusks:
cs.append([4051, 463, 847, 2498, 1811, 2157, 3629, 2462, 396, 4051])
cs.append([2816, 4318, 3669, 1003, 2588, 2182, 469, 2816])

zs = scipy.zeros((len(egE), len(cs)))

zp = []

for k in range(len(cs)):
    zp.append([])
    for i in range(len(cs[k])-1):
        if cs[k][i] < cs[k][i+1]:
            zs[egE.index((cs[k][i],cs[k][i+1])),k] = 1
            zp[k].append((cs[k][i],cs[k][i+1]))
        else:
            zs[egE.index((cs[k][i+1],cs[k][i])),k] = -1
            zp[k].append((cs[k][i+1],cs[k][i]))


egD = minimumhomology.boundaryedges(egP,egE).tocsc()
egDA1 = egD*scipy.diag(egAi)
egDA2 = egD*scipy.diag(scipy.sqrt(egAi))

chains = {}
for i in range(len(cs)):
    for j in range(i+1,len(cs)):
        try:
            chain = minimumhomology.boundingchain(egD,zs[:,i],zs[:,j])
            if not minimumhomology.connectedsupport(egP, chain):
                # puncture anything in the support
                skipI = chain.nonzero()[0][0]
                egDp = egD[:,range(skipI)+range(skipI+1,egD.shape[1])]
                chainP = minimumhomology.boundingchain(egDp,zs[:,i],zs[:,j])
                chain = scipy.r_[chainP[:skipI],0,chainP[skipI:]]
            chains[(i,j)] = chain
            print "Distance between %d and %d: %f" % (i, j, scipy.dot(abs(chain),egA))
        except minimumhomology.NonHomology:
            chains[(i,j)] = None
            print "Distance between %d and %d: %f" % (i, j, scipy.inf)
            

cols = {}
for k in chains:
    if chains[k] is None:
        cols[k] = [(1,1,1,0.7)]*len(egP)
        continue
    outside = abs(chains[k]).max()
    norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
    cols[k] = matplotlib.pyplot.cm.RdBu(norm(chains[k]))

for k in cols: 
    scipy.savetxt('smithsonian/mammoth-color-%d-%d.csv' % k, cols[k])


for k in cols:
    read3d.draw(egV, egP, facecolors=cols[k], edgecolors=(0,0,0,0.7), linewidths=0.2)
    ax = matplotlib.pyplot.gca()
    ax.view_init(20,-20)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.savefig('figs/mammoth-%d-%d.pdf' % k,bbox_inches='tight')
    matplotlib.pyplot.close()

for z in range(len(cs)):
    read3d.draw(egV, egP, facecolors=(1,1,1,.7), edgecolors=(0,0,0,0.7), linewidths=0.2)
    read3d.draw(egV,zp[z],edgecolors=(1,0,0),linewidths=2)
    ax = matplotlib.pyplot.gca()
    ax.view_init(20,-20)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.savefig('figs/mammoth-%d.pdf' % z,bbox_inches='tight')
    matplotlib.pyplot.close()
    
