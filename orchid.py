import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.sparse, scipy.sparse.linalg
import matplotlib
import networkx

eg = read3d.readply('ply/embreea.ply')
egV = eg[0]
egP = eg[1][:,1:]
egE = minimumhomology.edges(egP)


# patterns
# 0 0 0 255 z_1
# 0 0 255 255 z_2
# 0 255 0 255 z_3
# 255 255 255 255 rest

blues = list(scipy.loadtxt('embreea-blue.idx', dtype=scipy.int64)-15)
blacks = list(scipy.loadtxt('embreea-black.idx', dtype=scipy.int64)-15)
greens = list(scipy.loadtxt('embreea-green.idx', dtype=scipy.int64)-15)

blueE = [e for e in egE if e[0] in blues and e[1] in blues]
blackE = [e for e in egE if e[0] in blacks and e[1] in blacks]
greenE = [e for e in egE if e[0] in greens and e[1] in greens]

blueG = networkx.Graph()
blueG.add_edges_from(blueE)
blackG = networkx.Graph()
blackG.add_edges_from(blackE)
greenG = networkx.Graph()
greenG.add_edges_from(greenE)

blueCs = networkx.cycle_basis(blueG)
blackCs = networkx.cycle_basis(blackG)
greenCs = networkx.cycle_basis(greenG)

blueC = blueCs[scipy.array(map(len, blueCs)).argmax()]
blackC = blackCs[scipy.array(map(len, blackCs)).argmax()]
greenC = greenCs[scipy.array(map(len, greenCs)).argmax()]

blueZ = scipy.zeros((len(egE),))
for e in zip(blueC,blueC[1:]+[blueC[0]]):
    if e == sorted(e):
        c = 1
    else:
        c = -1
    blueZ[egE.index(tuple(sorted(e)))] = c

blackZ = scipy.zeros((len(egE),))
for e in zip(blackC,blackC[1:]+[blackC[0]]):
    if e == sorted(e):
        c = 1
    else:
        c = -1
    blackZ[egE.index(tuple(sorted(e)))] = c

greenZ = scipy.zeros((len(egE),))
for e in zip(greenC,greenC[1:]+[greenC[0]]):
    if e == sorted(e):
        c = 1
    else:
        c = -1
    greenZ[egE.index(tuple(sorted(e)))] = c

scipy.savetxt('blue_orchid.csv', blueZ)
scipy.savetxt('black_orchid.csv', blackZ)
scipy.savetxt('green_orchid.csv', greenZ)

    
# Cycles all setup, let's compute some homologies
egD = minimumhomology.boundaryedges(egP,egE)

chBK = minimumhomology.boundingchain(egD,blueZ,blackZ)
if not minimumhomology.connectedsupport(egP, chBK):
    skipI = chBK.nonzero()[0][0]
    egDp = egD.tocsc()[:,range(skipI)+range(skipI+1,egD.shape[1])]
    chainP = minimumhomology.boundingchain(egDp,z1,z2)
    chBK = scipy.r_[chainP[:skipI],0,chainP[skipI:]]


chBG = minimumhomology.boundingchain(egD,blueZ,greenZ)
if not minimumhomology.connectedsupport(egP, chBG):
    skipI = chBG.nonzero()[0][0]
    egDp = egD.tocsc()[:,range(skipI)+range(skipI+1,egD.shape[1])]
    chainP = minimumhomology.boundingchain(egDp,z1,z2)
    chBG = scipy.r_[chainP[:skipI],0,chainP[skipI:]]


chKG = minimumhomology.boundingchain(egD,blackZ,greenZ)
if not minimumhomology.connectedsupport(egP, chKG):
    skipI = chKG.nonzero()[0][0]
    egDp = egD.tocsc()[:,range(skipI)+range(skipI+1,egD.shape[1])]
    chainP = minimumhomology.boundingchain(egDp,z1,z2)
    chKG = scipy.r_[chainP[:skipI],0,chainP[skipI:]]

