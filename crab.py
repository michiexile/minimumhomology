import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.linalg, scipy.sparse, scipy.sparse.linalg
import matplotlib

eg = read3d.readply('smithsonian/simplecrabA.ply')
egV = eg[0]
egP = eg[1][:,1:]
egE = minimumhomology.edges(egP)
try: 
    egA = minimumhomology.areas(egP, egV)
except NotImplementedError:
    egA = scipy.loadtxt('smithsonian/simplecrab.area')

egAi = 1./scipy.array(egA)
egAi[scipy.isinf(egAi)] = 0

cs = []
# Two cycles on the base of the claws:

cs.append([3065,2075,4720,534,1814,499,2080,2359,1666,3151,1084,2523,3650,1029,918,4777,2528,3065])

cs.append([4250,3940,3787,4320,2394,4378,3361,2028,3744,980,1544,3363,4494,3008,3433,2357,4250])

# One big wobbly cycle towards back:

cs.append([3862,4932,2820,2063,1630,996,4341,3815,1262,4628,4821,1712,4446,2836,1705,606,3421,4527,2322,153,4669,4077,1241,3230,3610,1237,2038,3068,146,3854,1844,1472,599,1097,4428,1782,3297,3298,1485,4078,3305,1246,930,3313,3686,1690,1374,1691,3081,3952,1627,2828,2361,1172,4475,2620,4841,766,2822,30,578,3346,2340,3862])


# Should overlap several times with the above one, but is more diagonal:

cs.append([2247,3604,3348,4123,4689,996,1630,2436,3154,941,3986,2949,2590,2364,1104,4222,2322,2727,773,1996,243,2722,2456,2784,4057,4895,3658,2097,683,2757,706,3947,3287,2907,4067,177,734,2769,3105,690,1161,4810,2217,4814,404,4552,878,2137,2085,254,3204,1257,3699,3327,2143,1725,4831,4897,613,3920,2997,2344,3071,892,4318,2405,2247])

# And just for fun, a 4th different cycle that bisects the crab. Should be homologous just because this is a sphere, but not at all sure what result will be (or could totally fail if there is any hidden topology around the eyes, etc.)

#cs.append([2003,3690,1504,4446,3087,2945,3694,3549,436,2590,720,3856,747,3205,1375,3925,1258,846,549,3495,4101,4104,1623,4615,405,1499,2091,1618,1417,3844,930,4690,2447,2475,210,2061,4087,3926,4575,4810,2934,4806,1497,1144,4223,2325,252,4246,2081,1955,1868,1865,4816,99,539,1625,3560,1872,114,852,1724,827,4452,1787,4550,1200,1754,2003])



zs = scipy.zeros((len(egE), len(cs)))
zp = []

for k in range(len(cs)):
    zp.append([])
    for i in range(len(cs[k])-1):
        if cs[k][i] < cs[k][i+1]:
            zs[egE.index((cs[k][i],cs[k][i+1])),k] = 1
            zp[k].append((cs[k][i],cs[k][i+1]))
        else:
            zs[egE.index((cs[k][i+1],cs[k][i])),k] = -1
            zp[k].append((cs[k][i+1],cs[k][i]))

#zs = scipy.c_[zs[:,0]+zs[:,1], zs[:,2:]]
#zp = [zp[0]+zp[1], zp[2], zp[3]]

egD = minimumhomology.boundaryedges(egP,egE).tocsc()
egDA1 = egD*scipy.diag(egAi)
egDA2 = egD*scipy.diag(scipy.sqrt(egAi))

chains = {}
for i in range(zs.shape[1]):
    for j in range(i+1,zs.shape[1]):
        try:
            chain = minimumhomology.boundingchain(egD,zs[:,i],zs[:,j])
            if not minimumhomology.connectedsupport(egP, chain):
                # puncture anything in the support
                skipI = chain.nonzero()[0][0]
                egDp = egD[:,range(skipI)+range(skipI+1,egD.shape[1])]
                chainP = minimumhomology.boundingchain(egDp,zs[:,i],zs[:,j])
                chain = scipy.r_[chainP[:skipI],0,chainP[skipI:]]
            chains[(i,j)] = chain
            print "Distance between %d and %d: %f" % (i, j, scipy.dot(abs(chain),egA))
        except minimumhomology.NonHomology:
            chains[(i,j)] = None
            print "Distance between %d and %d: %f" % (i, j, scipy.inf)
            

cols = {}
for k in chains:
    if chains[k] is None:
        cols[k] = [(1,1,1,0.7)]*len(egP)
        continue
    outside = abs(chains[k]).max()
    norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
    cols[k] = matplotlib.pyplot.cm.RdBu(norm(chains[k]))

for k in cols: 
    scipy.savetxt('smithsonian/crab-color-%d-%d.csv' % k, cols[k])


for k in cols:
    read3d.draw(egV, egP, facecolors=cols[k], edgecolors=(0,0,0,0.7), linewidths=0.2)
    ax = matplotlib.pyplot.gca()
    ax.view_init(80,50)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.savefig('figs/crab-%d-%d.pdf' % k,bbox_inches='tight')
    matplotlib.pyplot.close()

for z in range(zs.shape[1]):
    read3d.draw(egV, egP, facecolors=(1,1,1,.7), edgecolors=(0,0,0,0.7), linewidths=0.2)
    read3d.draw(egV,zp[z],edgecolors=(1,0,0),linewidths=2)
    ax = matplotlib.pyplot.gca()
    ax.view_init(-95,95)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.savefig('figs/crab-%d.pdf' % z,bbox_inches='tight')
    matplotlib.pyplot.close()
    
