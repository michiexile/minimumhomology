import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.linalg, scipy.sparse, scipy.sparse.linalg
import matplotlib

eg = read3d.readply('smithsonian/simplerchair.ply')
egV = eg[0]
egP = eg[1][:,1:]
egE = minimumhomology.edges(egP)
try: 
    egA = minimumhomology.areas(egP, egV)
except NotImplementedError:
    egA = scipy.loadtxt('smithsonian/simplerchair.area')

egAi = 1./scipy.array(egA)
egAi[scipy.isinf(egAi)] = 0

cs = []
# Two cycles at the base of the chair back, above the seat:
cs.append([634, 422, 2405, 1654, 891, 1301, 1482, 1409, 634])
cs.append([2109, 505, 626, 2080, 61, 1881, 2347, 2039, 2109]) # 2309 doesn't connect to these

# And another in the middle, which should be homologous to the union of the above two:
cs.append([802, 1628, 1783, 1746, 1961, 2364, 912, 228, 1580, 1958, 33, 2293, 1341, 802])

# Near top of chair back (should be homologous to both of the others): 
cs.append([702, 1589, 1847, 819, 1090, 1875, 425, 2129, 539, 1338, 1646, 1755, 2383, 1112, 168, 1678, 162, 697, 1198, 220, 778, 702])

zs = scipy.zeros((len(egE), len(cs)))
zp = []

for k in range(len(cs)):
    zp.append([])
    for i in range(len(cs[k])-1):
        if cs[k][i] < cs[k][i+1]:
            zs[egE.index((cs[k][i],cs[k][i+1])),k] = 1
            zp[k].append((cs[k][i],cs[k][i+1]))
        else:
            zs[egE.index((cs[k][i+1],cs[k][i])),k] = -1
            zp[k].append((cs[k][i+1],cs[k][i]))

zs = scipy.c_[zs[:,0]+zs[:,1], zs[:,2:]]
zp = [zp[0]+zp[1], zp[2], zp[3]]

egD = minimumhomology.boundaryedges(egP,egE).tocsc()
egDA1 = egD*scipy.diag(egAi)
egDA2 = egD*scipy.diag(scipy.sqrt(egAi))

chains = {}
for i in range(zs.shape[1]):
    for j in range(i+1,zs.shape[1]):
        try:
            chain = minimumhomology.boundingchain(egD,zs[:,i],zs[:,j])
            if not minimumhomology.connectedsupport(egP, chain):
                # puncture anything in the support
                skipI = chain.nonzero()[0][0]
                egDp = egD[:,range(skipI)+range(skipI+1,egD.shape[1])]
                chainP = minimumhomology.boundingchain(egDp,zs[:,i],zs[:,j])
                chain = scipy.r_[chainP[:skipI],0,chainP[skipI:]]
            chains[(i,j)] = chain
            print "Distance between %d and %d: %f" % (i, j, scipy.dot(abs(chain),egA))
        except minimumhomology.NonHomology:
            chains[(i,j)] = None
            print "Distance between %d and %d: %f" % (i, j, scipy.inf)
            

cols = {}
for k in chains:
    if chains[k] is None:
        cols[k] = [(1,1,1,0.7)]*len(egP)
        continue
    outside = abs(chains[k]).max()
    norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
    cols[k] = matplotlib.pyplot.cm.RdBu(norm(chains[k]))

for k in cols: 
    scipy.savetxt('smithsonian/chair-color-%d-%d.csv' % k, cols[k])


for k in cols:
    read3d.draw(egV, egP, facecolors=cols[k], edgecolors=(0,0,0,0.7), linewidths=0.2)
    ax = matplotlib.pyplot.gca()
    ax.view_init(-95,95)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.savefig('figs/chair-%d-%d.pdf' % k,bbox_inches='tight')
    matplotlib.pyplot.close()

for z in range(zs.shape[1]):
    read3d.draw(egV, egP, facecolors=(1,1,1,.7), edgecolors=(0,0,0,0.7), linewidths=0.2)
    read3d.draw(egV,zp[z],edgecolors=(1,0,0),linewidths=2)
    ax = matplotlib.pyplot.gca()
    ax.view_init(-95,95)
    matplotlib.pyplot.axis('off')
    matplotlib.pyplot.savefig('figs/chair-%d.pdf' % z,bbox_inches='tight')
    matplotlib.pyplot.close()
    
