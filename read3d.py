from scipy import array, int64
from mpl_toolkits import mplot3d
from matplotlib import pyplot as plt

def readply(filename):
    fh = open(filename)
    vcount = 0
    fcount = 0
    for line in fh:
        token = line.split()
        if token[0] in ['ply', 'comment']:
            continue
        if token[0] == 'end_header':
            break
        if token[0] == 'element' and token[1] == 'vertex':
            vcount = int(token[2])
        if token[0] == 'element' and token[1] == 'face':
            fcount = int(token[2])
    vertices = []
    faces = []
    for c in range(vcount):
        line = fh.next()
        vertices.append(map(float, line.split()))
    for c in range(fcount):
        line = fh.next()
        faces.append(map(int, line.split()))
    fh.close()
    return (array(vertices), array(faces,dtype=int64))


def readobj(filename):
    fh = open(filename)
    vertices = []
    faces = []
    for line in fh:
        token = line.split()
        if token[0] == 'v':
            # adding a vertex
            vertices.append(map(float, token[1:]))
        if token[0] == 'f':
            # adding a face
            faces.append(map(lambda t: int(t.split('/')[0]), token[1:]))
    fh.close()
    return (array(vertices), array(faces,dtype=int16))


def draw(vertices, polys, fig=None, **kwargs):
    if fig == None:
        fig = plt.gcf()
    if len(fig.axes) == 0 or plt.gca().name != '3d':
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = plt.gca()
    ax.add_collection(mplot3d.art3d.Poly3DCollection([[vertices[i,:] for i in t] for t in polys],**kwargs))
    xs = vertices[:,0]
    ys = vertices[:,1]
    zs = vertices[:,2]
    xmin,xmax = xs.min(),xs.max()
    ymin,ymax = ys.min(),ys.max()
    zmin,zmax = zs.min(),zs.max()
    span = max([xmax-xmin,ymax-ymin,zmax-zmin])/2
    ax.set_xlim((xmin+xmax)/2-span,(xmin+xmax)/2+span)
    ax.set_ylim((ymin+ymax)/2-span,(ymin+ymax)/2+span)
    ax.set_zlim((zmin+zmax)/2-span,(zmin+zmax)/2+span)
    
