import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.sparse, scipy.sparse.linalg
import matplotlib

eg = read3d.readply('ply/sandal.ply')
egV = eg[0]
egP = eg[1][:,1:]
egE = minimumhomology.edges(egP)


c1 = [2238, 2227, 2226, 2225, 1988, 2211, 2203, 2200, 2198, 2199, 2261, 2349, 2342, 2362, 2260, 2238]
c2 = [2086, 2085, 2084, 2016, 2015, 2075, 2074, 2073, 2071, 2072, 2298, 2324, 2305, 2306, 2290, 2086]


z1 = scipy.zeros((len(egE),))
z2 = scipy.zeros((len(egE),))

z1p = []
z2p = []

for i in range(len(c1)-1):
    if c1[i] < c1[i+1]:
        z1[egE.index((c1[i],c1[i+1]))] = 1
        z1p.append((c1[i],c1[i+1]))
    else:
        z1[egE.index((c1[i+1],c1[i]))] = -1
        z1p.append((c1[i+1],c1[i]))
    

for i in range(len(c2)-1):
    if c2[i] < c2[i+1]:
        z2[egE.index((c2[i],c2[i+1]))] = 1
        z2p.append((c2[i],c2[i+1]))
    else:
        z2[egE.index((c2[i+1],c2[i]))] = -1
        z2p.append((c2[i+1],c2[i]))


egD = minimumhomology.boundaryedges(egP,egE)
chain = minimumhomology.boundingchain(egD,z1,z2)

if not minimumhomology.connectedsupport(egP, chain):
    # puncture anything in the support
    skipI = chain.nonzero()[0][0]
    egDp = egD.tocsc()[:,range(skipI)+range(skipI+1,egD.shape[1])]
    chainP = minimumhomology.boundingchain(egDp,z1,z2)
    chain = scipy.r_[chainP[:skipI],0,chainP[skipI:]]

outside = abs(chain).max()
norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
cols = matplotlib.pyplot.cm.RdBu(norm(chain))
read3d.draw(egV,egP,facecolors=cols,linewidths=0.2)

read3d.draw(egV,z1p,edgecolors=(1,0,0),linewidths=2)
read3d.draw(egV,z2p,edgecolors=(0,1,0),linewidths=2)

matplotlib.pyplot.show()
