import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.sparse, scipy.sparse.linalg
from scipy import pi, sin, cos, zeros
from matplotlib import cm

r = 1.
R = 10.
Ni = 100
Nj = 10

def toruspt(i,j):
    t = i*2*pi/Ni
    u = j*2*pi/Nj
    x = cos(t)*(R+r*cos(u))
    y = sin(t)*(R+r*cos(u))
    z = r*sin(u)
    return [x,y,z]
    

tV = []
tI = []
for i in range(Ni):
    for j in range(Nj):
        tV.append(toruspt(i,j))
        tI.append((i,j))
tV = scipy.array(tV)

tP = []
for i in range(Ni):
    for j in range(Nj):
        tP.append((tI.index((i,j)),tI.index((i,(j+1)%Nj)),
                   tI.index(((i+1)%Ni,(j+1)%Nj)),tI.index(((i+1)%Ni,j))))
tP = scipy.array(tP, dtype=scipy.int32)

tE = minimumhomology.edges(tP)

z1 = zeros((len(tE),))
z1p = []
for j in range(Nj-1):
    z1[tE.index((tI.index((0,j)),tI.index((0,j+1))))] = 1
    z1p.append((tI.index((0,j)),tI.index((0,j+1))))
z1[tE.index((tI.index((0,0)),tI.index((0,Nj-1))))] = -1

z2 = zeros((len(tE),))
z2p = []
for j in range(Nj-1):
    z2[tE.index((tI.index((45,j)),tI.index((45,j+1))))] = 1
    z2p.append((tI.index((45,j)),tI.index((45,j+1))))
z2[tE.index((tI.index((45,0)),tI.index((45,Nj-1))))] = -1


z3 = zeros((len(tE),))
z3p = []
for j in range(Nj-1):
    z3[tE.index((tI.index((55,j)),tI.index((55,j+1))))] = 1
    z3p.append((tI.index((55,j)),tI.index((55,j+1))))
z3[tE.index((tI.index((55,0)),tI.index((55,Nj-1))))] = -1


tA = scipy.array(minimumhomology.areas(tP,tV))
tD = minimumhomology.boundaryedges(tP,tE)
tAm = diag(1./sqrt(tA))
chainc = minimumhomology.weightedboundingchain(tD.todense(), z1, z2, tA)
chain = minimumhomology.boundingchain(tD,z1,z2)

outside = abs(chain).max()
norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
cols = cm.RdBu(norm(chain))
cols[:,3] = 0.5
read3d.draw(tV,tP,facecolors=cols,linewidths=0.2)

read3d.draw(tV,z1p,edgecolors=(1,0,0),linewidths=5)
read3d.draw(tV,z2p,edgecolors=(0,1,0),linewidths=5)
#read3d.draw(tV,z3p,edgecolors=(1,0,1),linewidths=5)
