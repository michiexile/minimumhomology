import read3d, minimumhomology, matplotlib.colors

wheel = read3d.readply('ply/steeringweel.ply')
whV = wheel[0]
whP = wheel[1][:,1:]
whE = minimumhomology.edges(whP)

read3d.draw(whV,whP,facecolors=(0,0,1,0.2),linewidths=0.2)

z1d = {(415,438):1, (396,438):-1, (395,396):-1, (394,395):-1, (334,394):-1, (272,334):-1, (272,279):1, (279,283):1, (283,348):1, (348,415):1}
z1 = zeros((len(whE),))
for k in z1d:
    z1[whE.index(k)] = z1d[k]

z2d = {(300,301):-1, (300,302):1, (302,328):1, (328,376):1, (376,377):1, (377,381):1, (381,432):1, (421,432):-1, (358,421):-1, (301,358):-1}
z2 = zeros((len(whE),))
for k in z2d:
    z2[whE.index(k)] = z2d[k]

whD = minimumhomology.boundaryedges(whP,whE)
chain = minimumhomology.boundingchain(whD,z1,z2)

outside = abs(chain).max()
norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
cols = cm.RdBu(norm(chain))
read3d.draw(whV,whP,facecolors=cols,linewidths=0.2)

read3d.draw(whV,z1d,edgecolors=(1,0,0),linewidths=2)
read3d.draw(whV,z2d,edgecolors=(0,1,0),linewidths=2)
