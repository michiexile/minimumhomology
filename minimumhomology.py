from scipy import *
import scipy as sp
import scipy.sparse as sps
import scipy.sparse.linalg as linalg
import scipy.optimize as opt
import itertools
from collections import defaultdict

class NonHomology(Exception):
    pass


try:
    import networkx

    
    def connectedsupport(polys, chain):
        G = networkx.Graph()
        support = chain.nonzero()[0]
        for i in support:
            map(lambda e: G.add_edge(*e), edges([polys[i]]))
        return networkx.connected.is_connected(G)
except ImportError:
    def connectedsupport(polys, chain):
        raise NotImplementedError("Connected support not implemented. Install networkx.")
    
try:
    from shapely import geometry
    def areas(polys, verts):
        return [geometry.Polygon(map(lambda s: verts[s], list(p))).area for p in polys]
except ImportError:
    def areas(polys, verts):
        raise NotImplementedError("Area computation not implemented. Install shapely.")
    

def countcofaces(polys):
    es = defaultdict(int)
    for t in polys:
        for i in range(len(t)):
            for j in range(i+1,len(t)):
                es[tuple(sorted([t[i],t[j]]))] += 1
    return es

def edges(polys):
    es = set()
    for t in polys:
        for i in range(len(t)):
            es.add(tuple(sorted([t[i],t[(i+1)%len(t)]])))
    return sorted(es)

def boundaryvertices(edges, vertices, dtype=sp.float64):
    ii = []
    jj = []
    vv = []
    for ei, e in enumerate(edges):
        if e[0] in vertices and e[1] in vertices:
            vip = vertices.index(e[0])
            vim = vertices.index(e[1])
            ii.append(vip)
            jj.append(ei)
            vv.append(1)
            ii.append(vim)
            jj.append(ei)
            vv.append(-1)
    return sps.coo_matrix((vv,(ii,jj)), shape=(len(vertices),len(edges)), dtype=dtype)
            

def boundaryedges(polys, edges, dtype=sp.float64):
    ii = []
    jj = []
    vv = []
    for ti,t in enumerate(polys):
        for i in range(len(t)):
            e = (t[i],t[(i+1)%len(t)])
            if e in edges:
                k = edges.index(e)
                ii.append(k)
                jj.append(ti)
                vv.append(1)
            else:
                k = edges.index((e[1],e[0]))
                ii.append(k)
                jj.append(ti)
                vv.append(-1)
    return sps.coo_matrix((vv,(ii,jj)),shape=(len(edges),len(polys)),dtype=dtype)

def boundarymap(polys,dtype=sp.float16):
    return boundaryedges(polys, edges(polys),dtype=dtype)

def boundingchain(D,z,w,wt=None):
    """
    Guaranteed to give us minimum L2 norm solution -- i.e. fewest cells.
    C.f. http://see.stanford.edu/materials/lsoeldsee263/08-min-norm.pdf page 4.
    """
    lsqr = linalg.lsqr(D, z-w)
    lsqrf = linalg.lsqr(D, z+w)
    if lsqr[1] != 1 and lsqrf[1] != 1:
        raise NonHomology('Not homologous cycles')
    if lsqr[1] != 1:
        ret = array(map(round,lsqrf[0]))
    if lsqrf[1] != 1:
        ret = array(map(round,lsqr[0]))
    if lsqr[8] < lsqrf[8]:
        ret = array(map(round,lsqr[0]))
    else:
        ret = array(map(round,lsqrf[0]))
    
    return ret

try:
    from sklearn import linear_model
    def weightedboundingchain(D,z,w,wt=None):
        """
        Guaranteed to give minimum L1-norm solution, optionally with rescaling from wt.
        """
        if sps.issparse(D):
            tD = D.A
        else:
            tD = D
        if wt == None:
            tD = tD
        else:
            tD = tD*1./array(wt)
        omp = linear_model.OrthogonalMatchingPursuit(tol=1e-4)
        omp.fit(tD,z-w)
        return omp.coef_/array(wt)
except ImportError:
    def weightedboundingchain(D,z,w,wt=None):
        raise NotImplementedError("Weighted bounding chain not implemented. Install scikit-learn.")
