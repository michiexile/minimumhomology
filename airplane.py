import read3d, minimumhomology, matplotlib.colors
import scipy, scipy.sparse, scipy.sparse.linalg

ap = read3d.readply('ply/airplane.ply')
apV = ap[0]
apP = ap[1][:,1:]
apE = minimumhomology.edges(apP)


c1 = [297,892,886,880,874,868,862,856,850,844,242,243,249,255,261,267,273,279,285,291,297]
c2 = [184,191,805,798,791,784,777,770,763,756,749,121,122,135,142,149,156,163,170,177,184]
c3 = [317,319,910,908,906,904,902,900,898,896,894,300,301,303,305,307,309,311,313,315,317]


z1 = zeros((len(apE),))
z2 = zeros((len(apE),))
z3 = zeros((len(apE),))

z1p = []
z2p = []
z3p = []

for i in range(len(c1)-1):
    if c1[i] < c1[i+1]:
        z1[apE.index((c1[i],c1[i+1]))] = 1
        z1p.append((c1[i],c1[i+1]))
    else:
        z1[apE.index((c1[i+1],c1[i]))] = -1
        z1p.append((c1[i+1],c1[i]))
    

for i in range(len(c2)-1):
    if c2[i] < c2[i+1]:
        z2[apE.index((c2[i],c2[i+1]))] = 1
        z2p.append((c2[i],c2[i+1]))
    else:
        z2[apE.index((c2[i+1],c2[i]))] = -1
        z2p.append((c2[i+1],c2[i]))

for i in range(len(c3)-1):
    if c3[i] < c3[i+1]:
        z3[apE.index((c3[i],c3[i+1]))] = 1
        z3p.append((c3[i],c3[i+1]))
    else:
        z3[apE.index((c3[i+1],c3[i]))] = -1
        z3p.append((c3[i+1],c3[i]))

apD = minimumhomology.boundaryedges(apP,apE)
chain = minimumhomology.boundingchain(apD,z2,z3)

outside = abs(chain).max()
norm = matplotlib.colors.Normalize(vmin=-outside,vmax=outside)
cols = cm.RdBu(norm(chain))
read3d.draw(apV,apP,facecolors=cols,linewidths=0.2)

read3d.draw(apV,z2p,edgecolors=(1,0,0),linewidths=2)
read3d.draw(apV,z3p,edgecolors=(0,1,0),linewidths=2)

